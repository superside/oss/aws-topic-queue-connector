package com.superside.topicqueueconnector.annotation

/**
 * Creates a queue and a topic if they don't exist, subscribes the queue to the topic
 * and adds annotated method as a listener for queue events with passed subject
 *
 * Note:
 * 1. Topic and queue should have the same fifo or not fifo type
 * 2. Not all regions support FIFO
 * 3. Queue and topic arguments can be properties file parameter names "/${some.queue.name}" or names itself "queue-name"
 */
@Target(AnnotationTarget.FUNCTION)
annotation class SQSBasedTopicListener(
    val topic: String,
    val subject: String,
    val queue: String
)
