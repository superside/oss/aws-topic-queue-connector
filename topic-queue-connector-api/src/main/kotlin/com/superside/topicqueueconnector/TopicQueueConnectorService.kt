package com.superside.topicqueueconnector

import com.amazonaws.services.sns.model.Topic

interface TopicQueueConnectorService {
    fun connectQueueToTopicAndAddQueueListener(
        topicName: String,
        subject: String,
        queueName: String,
        listener: (payload: String) -> Unit
    )

    fun findOrCreateTopic(topicName: String): Topic
}
