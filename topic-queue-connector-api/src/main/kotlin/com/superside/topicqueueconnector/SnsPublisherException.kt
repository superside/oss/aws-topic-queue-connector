package com.superside.topicqueueconnector

class SnsPublisherException(message: String, cause: Throwable? = null) : RuntimeException(message, cause)
