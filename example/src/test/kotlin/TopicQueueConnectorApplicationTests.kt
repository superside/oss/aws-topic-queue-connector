import com.superside.example.ExampleApplication
import com.superside.example.ExampleEvent
import com.superside.example.ExampleListener
import com.superside.example.ExampleRestController
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.Mockito.verify
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.SpyBean
import org.springframework.test.context.ActiveProfiles

@SpringBootTest(classes = [ExampleApplication::class])
@ActiveProfiles("test")
@Disabled
class TopicQueueConnectorApplicationTests {

    @Autowired
    private lateinit var exampleRestController: ExampleRestController

    @SpyBean
    private lateinit var exampleListener: ExampleListener

    @Test
    fun `context loads`() {}

    @Test
    fun `send and receive success`() {
        repeat(10) { exampleRestController.sendSuccess() }
        (1..10).forEach {
            verify(exampleListener, Mockito.timeout(10000).times(1)).listener(ExampleEvent("hello", it))
        }
    }

    @Test
    fun `send and receive success - fifo`() {
        repeat(10) { exampleRestController.sendSuccessFifo() }
        (1..10).forEach {
            verify(exampleListener, Mockito.timeout(10000).times(1)).fifoListener(ExampleEvent("hello fifo", it))
        }
    }

    @Test
    fun `send and receive all fail`() {
        repeat(10) { exampleRestController.sendFail() }
        verify(exampleListener, Mockito.timeout(10000).times(10)).failListener(mapOf("any" to "thing"))
    }

    @Test
    fun `send and receive only first fail - fifo`() {
        repeat(2) { exampleRestController.sendFailFifo() }
        verify(exampleListener, Mockito.timeout(10000).times(1)).failFifoListener(mapOf("any" to "thing fifo"))
    }
}
