package com.superside.example

import com.superside.topicqueueconnector.TopicQueueConnectorModule
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Import

@SpringBootApplication(exclude = [DataSourceAutoConfiguration::class, HibernateJpaAutoConfiguration::class])
@Import(TopicQueueConnectorModule::class)
open class ExampleApplication

fun main(args: Array<String>) {
    runApplication<ExampleApplication>(*args)
}
