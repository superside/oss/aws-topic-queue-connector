package com.superside.example

data class ExampleEvent(
    val message: String,
    val num: Int
) {
    override fun toString(): String {
        return "ExampleEvent1(message:[$message],num:[$num])"
    }
}
