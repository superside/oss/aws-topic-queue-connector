package com.superside.example

import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.sns.AmazonSNS
import com.amazonaws.services.sns.AmazonSNSClientBuilder
import com.amazonaws.services.sqs.AmazonSQS
import com.amazonaws.services.sqs.AmazonSQSClientBuilder
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
open class DefaultAWSClients {

    @Bean
    open fun snsClient(): AmazonSNS {
        return AmazonSNSClientBuilder.defaultClient()
    }

    @Bean
    open fun sqsClient(): AmazonSQS {
        return AmazonSQSClientBuilder.defaultClient()
    }

    @Bean
    open fun s3Client(): AmazonS3 {
        return AmazonS3ClientBuilder.defaultClient()
    }
}
