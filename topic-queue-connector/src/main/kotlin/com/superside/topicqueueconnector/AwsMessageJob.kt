package com.superside.topicqueueconnector

import com.amazonaws.services.sqs.model.Message
import java.util.function.Consumer

data class AwsMessageJob(
    val message: Message,
    val onSuccess: Consumer<AwsMessageJob>,
)
