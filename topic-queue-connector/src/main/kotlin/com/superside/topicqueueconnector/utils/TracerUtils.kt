package com.superside.topicqueueconnector.utils

import io.opentracing.util.GlobalTracer

internal object TracerUtils {
    fun addTagToSpan(messageId: String) {
        val activeSpan = GlobalTracer.get().activeSpan()
        if (activeSpan != null) {
            activeSpan.setTag("messageId", messageId)
        } else {
            val span = GlobalTracer.get()
                .buildSpan("processMessageAsync")
                .withTag("messageId", messageId)
                .start()
            GlobalTracer.get().activateSpan(span)
        }
    }
}
