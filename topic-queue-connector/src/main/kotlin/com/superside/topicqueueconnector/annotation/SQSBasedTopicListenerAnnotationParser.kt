package com.superside.topicqueueconnector.annotation

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import com.superside.topicqueueconnector.TopicQueueConnectorService
import com.superside.topicqueueconnector.utils.InstantAdapter
import com.superside.topicqueueconnector.utils.LocalDateTimeAdapter
import com.superside.topicqueueconnector.utils.UUIDAdapter
import org.slf4j.LoggerFactory
import org.springframework.aop.support.AopUtils
import org.springframework.beans.factory.config.BeanPostProcessor
import org.springframework.core.MethodIntrospector
import org.springframework.core.annotation.AnnotatedElementUtils
import org.springframework.core.env.ConfigurableEnvironment
import org.springframework.stereotype.Component
import java.lang.reflect.Method
import java.util.concurrent.ConcurrentHashMap

@Component
class SQSBasedTopicListenerAnnotationParser(
    private val topicQueueConnectorService: TopicQueueConnectorService,
    private val configurableEnvironment: ConfigurableEnvironment
) : BeanPostProcessor {
    private val nonAnnotatedClasses: Set<Class<*>> = ConcurrentHashMap.newKeySet()

    override fun postProcessAfterInitialization(bean: Any, beanName: String): Any {
        if (bean.javaClass in nonAnnotatedClasses) {
            return bean
        }
        val targetClass = AopUtils.getTargetClass(bean)
        val annotatedMethods =
            MethodIntrospector.selectMethods<Set<SQSBasedTopicListener>>(targetClass) { findListenerAnnotations(it) }

        if (annotatedMethods.isEmpty()) {
            nonAnnotatedClasses.plus(bean.javaClass)
        } else {
            annotatedMethods.forEach { (method: Method, listenersAnnotations: Set<SQSBasedTopicListener>) ->
                val listener = fun(payload: String) {
                    val adapter = moshi.adapter(method.parameterTypes.first())
                    val event = adapter.fromJson(payload)
                    method.invoke(bean, event)
                }
                listenersAnnotations.forEach { annotation ->
                    val topicName = configurableEnvironment.resolvePlaceholders(annotation.topic)
                    val queueName = configurableEnvironment.resolvePlaceholders(annotation.queue)
                    log.info("Connecting topic [$topicName] to queue [$queueName] with subject [${annotation.subject}]")
                    topicQueueConnectorService.connectQueueToTopicAndAddQueueListener(
                        topicName = topicName,
                        subject = annotation.subject,
                        queueName = queueName,
                        listener = listener,
                    )
                }
            }
        }
        return bean
    }

    private fun findListenerAnnotations(method: Method): Set<SQSBasedTopicListener> {
        val annotation: SQSBasedTopicListener? = AnnotatedElementUtils.findMergedAnnotation(method, SQSBasedTopicListener::class.java)
        return annotation?.let { setOf(it) } ?: emptySet()
    }

    companion object {
        private val moshi: Moshi = Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .add(LocalDateTimeAdapter())
            .add(UUIDAdapter())
            .add(InstantAdapter())
            .build()

        private val log = LoggerFactory.getLogger(SQSBasedTopicListenerAnnotationParser::class.java)
    }
}
