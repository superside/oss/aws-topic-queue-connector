package com.superside.topicqueueconnector

import com.amazonaws.arn.Arn
import com.amazonaws.auth.policy.Policy
import com.amazonaws.auth.policy.Principal
import com.amazonaws.auth.policy.Resource
import com.amazonaws.auth.policy.Statement
import com.amazonaws.auth.policy.actions.SQSActions
import com.amazonaws.auth.policy.conditions.ConditionFactory
import com.amazonaws.services.sns.AmazonSNS
import com.amazonaws.services.sns.model.CreateTopicRequest
import com.amazonaws.services.sns.model.Topic
import com.amazonaws.services.sqs.AmazonSQS
import com.amazonaws.services.sqs.model.CreateQueueRequest
import com.amazonaws.services.sqs.model.GetQueueAttributesRequest
import com.amazonaws.services.sqs.model.QueueAttributeName
import com.amazonaws.services.sqs.model.SetQueueAttributesRequest
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.util.concurrent.ConcurrentHashMap
import javax.annotation.PostConstruct

@Service
class TopicQueueConnectorServiceBean(
    private val sqsClient: AmazonSQS,
    private val snsClient: AmazonSNS,
    private val messagesProcessor: MessagesProcessor,
) : TopicQueueConnectorService {

    @PostConstruct
    fun init() {
        allSnsTopics.addAll(snsClient.listTopics().topics)
    }

    override fun connectQueueToTopicAndAddQueueListener(
        topicName: String, subject: String, queueName: String, listener: (payload: String) -> Unit
    ) {
        val topic = findOrCreateTopic(topicName)
        val queueUrl = findOrCreateQueue(sqsName = queueName, dlqName = queueName.toDLQName())
        subscribeQueueOnTopic(queueName, topic)
        messagesProcessor.addListener(TopicArnWithSubjectAndQueue(topic.topicArn, subject, queueUrl), listener)
        log.info("Queue [$queueName] connected to [$topicName], listener for queue added with filtering by [$subject]")
    }

    override fun findOrCreateTopic(topicName: String): Topic {
        var topic = snsTopicCache[topicName] ?: allSnsTopics.find { it.topicArn.endsWith(":$topicName") }
        if (topic == null) {
            val fifoAttribute = topicName.getFifoAttribute()
            val createTopicResult =
                snsClient.createTopic(CreateTopicRequest(topicName).addAttributesEntry("FifoTopic", fifoAttribute))
            val arn = createTopicResult.topicArn
            topic = Topic().withTopicArn(arn)
            allSnsTopics.add(topic)
        }
        snsTopicCache[topicName] = topic!!
        return topic
    }

    private fun subscribeQueueOnTopic(queueUrl: String, topic: Topic) {
        val sqsAttrNames = listOf(QueueAttributeName.QueueArn.toString())
        val sqsAttrs = sqsClient.getQueueAttributes(queueUrl, sqsAttrNames).attributes
        val endpoint = sqsAttrs[QueueAttributeName.QueueArn.toString()]
        snsClient.subscribe(topic.topicArn, "sqs", endpoint)
    }

    private fun getArn(queueUrl: String): Arn {
        val deadLetterQueueAttributes = sqsClient.getQueueAttributes(
            GetQueueAttributesRequest(queueUrl)
                .withAttributeNames("QueueArn")
        )
        return Arn.fromString(deadLetterQueueAttributes.attributes["QueueArn"])
    }

    private fun Arn.setAsDlqFor(queueUrl: String) {
        val sqsAttrNames = listOf(QueueAttributeName.QueueArn.toString())
        val sqsAttrs: Map<String, String> = sqsClient.getQueueAttributes(queueUrl, sqsAttrNames).attributes
        val setAttributesRequest: SetQueueAttributesRequest = SetQueueAttributesRequest()
            .withQueueUrl(queueUrl)
            .addAttributesEntry(QueueAttributeName.ReceiveMessageWaitTimeSeconds.toString(), "20")
            .addAttributesEntry(QueueAttributeName.Policy.toString(), createPolicyJson(sqsAttrs, this))
            .addAttributesEntry(
                QueueAttributeName.RedrivePolicy.toString(),
                "{\"maxReceiveCount\":\"5\", \"deadLetterTargetArn\":\"$this\"}"
            )
        sqsClient.setQueueAttributes(setAttributesRequest)
    }

    private fun findOrCreateQueue(sqsName: String, dlqName: String): String {
        val fifoAttribute = sqsName.getFifoAttribute()
        val createQueueRequest = CreateQueueRequest(sqsName)
        if (fifoAttribute == TRUE) {
            createQueueRequest.addAttributesEntry(QueueAttributeName.FifoQueue.toString(), TRUE)
        }
        val queueUrl = sqsClient.createQueue(createQueueRequest).queueUrl
        val createDLQueueRequest = CreateQueueRequest(dlqName)
        if (fifoAttribute == TRUE) {
            createDLQueueRequest.addAttributesEntry(QueueAttributeName.FifoQueue.toString(), TRUE)
        }
        val deadLetterQueueUrl = sqsClient.createQueue(createDLQueueRequest).queueUrl
        getArn(deadLetterQueueUrl).setAsDlqFor(queueUrl)
        return queueUrl
    }

    companion object {
        private const val FIFO_SUFFIX = ".fifo"
        private const val DLQ_SUFFIX = "_dlq"
        private const val TRUE = "true"
        private const val FALSE = "false"
        private val log = LoggerFactory.getLogger(TopicQueueConnectorServiceBean::class.java)
        private var allSnsTopics = ConcurrentHashMap.newKeySet<Topic>()
        private var snsTopicCache = ConcurrentHashMap<String, Topic>()

        private fun String.getFifoAttribute(): String {
            return if (this.endsWith(FIFO_SUFFIX)) TRUE else FALSE
        }

        private fun String.toDLQName(): String {
            return if (this.endsWith(FIFO_SUFFIX)) {
                "${this.substringBefore(FIFO_SUFFIX)}$DLQ_SUFFIX$FIFO_SUFFIX"
            } else {
                "${this}_dlq"
            }
        }

        private fun createPolicyJson(sqsAttrs: Map<String, String>, deadLetterQueueArn: Arn): String =
            Policy().withStatements(
                Statement(Statement.Effect.Allow)
                    .withPrincipals(Principal.AllUsers)
                    .withResources(Resource(sqsAttrs[QueueAttributeName.QueueArn.toString()]))
                    .withActions(SQSActions.SendMessage)
                    .withConditions(
                        ConditionFactory.newSourceArnCondition(
                            "arn:aws:sns:${deadLetterQueueArn.region}:${deadLetterQueueArn.accountId}:*"
                        )
                    )
            ).toJson()
    }
}


