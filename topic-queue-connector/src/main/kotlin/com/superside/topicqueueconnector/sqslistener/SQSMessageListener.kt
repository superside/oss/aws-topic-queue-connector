package com.superside.topicqueueconnector.sqslistener

import com.amazonaws.AbortedException
import com.amazonaws.services.sqs.AmazonSQS
import com.amazonaws.services.sqs.model.Message
import com.amazonaws.services.sqs.model.MessageSystemAttributeName
import com.amazonaws.services.sqs.model.ReceiveMessageRequest
import com.superside.topicqueueconnector.AwsMessageJob
import com.superside.topicqueueconnector.utils.TracerUtils
import datadog.trace.api.Trace
import org.slf4j.LoggerFactory
import java.io.IOException
import java.util.function.Consumer

class SQSMessageListener(
    private val amazonSQS: AmazonSQS,
    val queueUrl: String,
    val consumer: Consumer<AwsMessageJob>,
    private val retriesLimit: Int,
    @Volatile
    private var applicationStarted: Boolean = false
) : Runnable {

    private var errors: Int = 0
    @Volatile
    private var stopped: Boolean = false

    fun shutdown() {
        log.info("Stopping listener for queue [$queueUrl]")
        stopped = true
    }

    fun afterApplicationStarted() {
        log.info("Application started, marking listener of queue [$queueUrl] as ready to consume")
        applicationStarted = true
    }

    fun healthy(): Boolean {
        return !stopped
    }

    override fun run() {
        log.info("Starting listener for queue [$queueUrl]")
        while (!applicationStarted) {
            log.info("Waiting for app startup to start consuming from [$queueUrl]")
            Thread.sleep(CHECK_APPLICATION_STARTED_EACH_MS)
        }
        while (!stopped) {
            val receiveMessageRequest = ReceiveMessageRequest(queueUrl).also { req ->
                req.waitTimeSeconds = RECEIVE_MESSAGE_WAIT_TIMEOUT_SECONDS
                req.setAttributeNames(listOf(MessageSystemAttributeName.ApproximateReceiveCount.toString()))
            }
            try {
                val result = amazonSQS.receiveMessage(receiveMessageRequest)
                log.debug("Starting processing [{}] messages from [{}]", result.messages.size, queueUrl)
                result.messages
                    .forEach { consume(it) }
            } catch (e: InterruptedException) {
                throw e
            } catch (e: AbortedException) {
                log.info("Stopping after $e")
                stopped = true
            } catch (e: Exception) {
                errors += 1
                log.error(
                    "Fail to receive messages from queue [$queueUrl], [$errors] error occurred in this listener",
                    e
                )
                if (errors > retriesLimit) {
                    log.error("Errors limit reached for queue [$queueUrl]")
                    stopped = true
                }
            }
        }
        log.info("Stopped listener for queue [$queueUrl]")
    }

    @Trace(resourceName = "SQSMessageListener.Message")
    private fun consume(message: Message) {
        try {
            log.debug(
                "Starting consuming message [{}] from queue [{}], current retry [{}]",
                message.messageId, queueUrl, message.currentRetry()
            )
            consumer.accept(
                AwsMessageJob(
                    message = message,
                    onSuccess = { awsMessageJob -> onSuccess(awsMessageJob) },
                )
            )
            TracerUtils.addTagToSpan(message.messageId)
        } catch (e: Exception) {
            if (e is InterruptedException) {
                throw e
            }
            log.error("Fail to parse message [${message.messageId}]", e)
        }
    }

    private fun onSuccess(awsMessageJob: AwsMessageJob) {
        amazonSQS.deleteMessage(queueUrl, awsMessageJob.message.receiptHandle)
        log.debug("Message [{}] successfully processed", awsMessageJob.message.messageId)
    }

    companion object {
        private fun Message.currentRetry(): Int? {
            return try {
                Integer.parseInt(this.attributes[MessageSystemAttributeName.ApproximateReceiveCount.toString()])
            } catch (e : IOException) {
                null
            }
        }
        private val log = LoggerFactory.getLogger(SQSMessageListener::class.java)
        private const val RECEIVE_MESSAGE_WAIT_TIMEOUT_SECONDS = 20
        private const val CHECK_APPLICATION_STARTED_EACH_MS = 10L * 1000L
    }
}
